export declare class GenericConsentField {
    val?: string;
    metadata: {
        time?: Date,
        id?: string,
        name?: string,
        description?: string,
        url?: string,
        archived?: boolean,
        expiration?: string
    };
}