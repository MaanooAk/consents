import { GenericConsentField } from './GenericConsentField';

export declare class GenericConsentFieldGroup {
    [k: string]: GenericConsentField;
    metadata: {
        name: string,
        id?: string,
        description?: string,
        archived?: boolean,
        url?: string,
    };
}

export declare interface GenericConsentFieldDocument {
    [k: string]: GenericConsentFieldGroup;
}