import { Router } from 'express';
import { UserConsentService } from './UserConsentService';
import { PatternValidator } from '@themost/data';
import { HttpBadRequestError } from '@themost/common';

// eslint-disable-next-line no-unused-vars
function userConsentServiceRouter(app) {
    const router = Router();

    router.get('/', async (req, res, next) => {
        try {
            const service = req.context.application.getService(UserConsentService);
            const value = await service.getConsentsForCurrentUser(req.context);
            return res.json({
                value
            });
        } catch (error) {
            return next(error);
        }
    });

    function validateGroup() {
        return (req, res, next) => {
            try {
                const { group } = req.params;
                const attribute = req.context.model('ConsentFieldGroup').getAttribute('alternateName');
                if (attribute.validation && attribute.validation.pattern) {
                    const validator = new PatternValidator(attribute.validation.pattern);
                    validator.setContext(req.context);
                    validator.message = 'Consent group name is not valid';
                    const validationResult = validator.validateSync(group);
                    if (validationResult) {
                        return next(new HttpBadRequestError(validationResult.message));
                    }
                }
            return next();
            } catch (error) {
                return next(error);
            }
        }
    }

    router.get('/:group', validateGroup(), async (req, res, next) => {
        try {
            const service = req.context.application.getService(UserConsentService);
            const value = await service.getConsentGroupForCurrentUser(req.context, req.params.group);
            return res.json({
                value
            });
        } catch (error) {
            return next(error);
        }
    });

    router.post('/:group', validateGroup(), async (req, res, next) => {
        try {
            const body = req.body;
            const items = [];
            const { group } = req.params;
            const user = await req.context.model('User').where('name').equal(req.context.user.name).select('id').value();
            const fieldGroup = await req.context.model('ConsentFieldGroup').asQueryable()
                .where('alternateName').equal(group)
                .expand('fields')
                .getItem();
            const fieldNames = fieldGroup.fields.map((f) => f.alternateName);
            const keys = Object.keys(body[group]);
            keys.filter((field) => {
                return field !== 'metadata' && Object.prototype.hasOwnProperty.call(body[group], field) && fieldNames.indexOf(field) >= 0;
            }).forEach((consent) => {
                const {val: value} = body[group][consent];
                items.push({
                    consent,
                    value,
                    user
                });
            });
            const saved = await req.context.model('UserConsent').save(items);
            return res.json({
                value: saved.length
            });
        } catch (error) {
            return next(error);
        }
    });

    router.post('/', async (req, res, next) => {
        try {
            // get body and extract consent fields
            /**
             * @type {import('./UserConsentService').UserConsentDocument}
             */
            const groups = req.body;
            const items = [];
            // get groups
            const fieldGroups = await req.context.model('ConsentFieldGroup').asQueryable().expand('fields').getAllItems();
            // get group names
            const groupNames = fieldGroups.map((g) => g.alternateName);
            const keys = Object.keys(groups);
            keys.filter((group) => {
                return Object.prototype.hasOwnProperty.call(groups, group) && groupNames.indexOf(group) >= 0;
            }).forEach((group) => {
                // get group
                const fieldGroup = fieldGroups.find((g) => g.alternateName === group);
                // get field names
                const fieldNames = fieldGroup.fields.map((f) => f.alternateName);
                const keys = Object.keys(groups[group]);
                keys.filter((field) => {
                    return field !== 'metadata' && Object.prototype.hasOwnProperty.call(groups[group], field) && fieldNames.indexOf(field) >= 0;
                }).forEach((consent) => {
                    const {val: value} = groups[group][consent];
                    const user = {
                        name: req.context.user.name,
                    }
                    items.push({
                        consent,
                        value,
                        user
                    });
                });
            });
            const saved = await req.context.model('UserConsent').save(items);
            return res.json({
                value: saved.length
            });
        } catch (error) {
            return next(error);
        }
    });

    return router;
}

export {
    userConsentServiceRouter
}