class GenericConsentFieldGroup {
    metadata = {
        id: null,
        name: null,
        description: null,
        archived: false,
        url: null
    }
}

class GenericConsentFieldGroupConverter {
    /**
     * 
     * @param {*} fieldGroup 
     */
    constructor(fieldGroup) {
        this.fieldGroup = fieldGroup;
    }

    /**
     * @param {Array<*>} values 
     */
    convert(values) {
        const { fieldGroup: group } = this;
        const { fields } = group;
        const result = {};
        if (Array.isArray(fields)) {
            // set group values
            const resultValues = fields.reduce((previous, field) => {
                // find user consent
                const value = values.find((item) => {
                    return item.consent === field.alternateName;
                });
                // get value or unknown
                const val = value ? value.value : field.value || 'u';
                // get last modified
                const time = value ? value.dateModified : undefined;
                // get field name and description from locale or field
                const metadata = field.locale || field;
                const { name, description } = metadata;
                const { archived, expiration } = field;
                Object.assign(previous, {
                    [field.alternateName]: {
                        val,
                        metadata: {
                            time,
                            name,
                            description,
                            archived,
                            expiration
                        }
                    }
                });
                return previous;
            }, {});
            // get last updated
            const names = fields.map((field) => field.alternateName);
            const [lastConsent] = values.filter((item) => names.includes(item.consent))
                .sort((a, b) => {
                    return a.dateModified > b.dateModified ? -1 : 1;
                });
            const time = lastConsent ? lastConsent.dateModified : undefined;
            // get group name and description from locale or field
            const metadata = group.locale || group;
            const { name, description } = metadata;
            const { archived, url } = group;
            result[group.alternateName] = Object.assign(resultValues, {
                metadata: {
                    time,
                    name,
                    description,
                    archived,
                    url
                }
            });
        }
        return result;
    }

}

export {
    GenericConsentFieldGroup,
    GenericConsentFieldGroupConverter
}