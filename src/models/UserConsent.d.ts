import { DataObject } from '@themost/data';

export declare class UserConsent extends DataObject {
    
    id?: string;
    consent?: string | any;
    user?: any;
    dateCreated?: Date;
    dateModified?: Date;
    createdBy?: any;
    modifiedBy?: any;
    consent?: any;

}