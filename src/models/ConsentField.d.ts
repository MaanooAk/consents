import { DataObject } from '@themost/data';
export declare class ConsentField extends DataObject {
    id?: string;
    name?: string;
    alternateName?: string;
    description?: string;
    url?: string;
    image?: string;
    archived?: boolean;
    value?: any;
    expiration?: string;
    group?: string | any;
    dateCreated?: Date;
    dateModified?: Date;
    createdBy?: any;
    modifiedBy?: any;
    locale?: {
        name?: string,
        description?: string
    };
    locales?: {
        name?: string,
        description?: string,
    }[];
}