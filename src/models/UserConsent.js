import { DataObject, EdmMapping } from '@themost/data';

@EdmMapping.entityType('UserConsent')
class UserConsent extends DataObject {
    constructor() {
        super();
    }
}

export {
    UserConsent
}