import { ApplicationService, DataNotFoundError } from '@themost/common';
import { userConsentServiceRouter } from './userConsentServiceRouter';
import { Router } from 'express';
import { GenericConsentFieldGroupConverter } from './GenericConsentFieldGroup';
import LocalScopeAccessConfiguration from './config/scope.access.json';
import '@themost/promise-sequence';

class UserConsentService extends ApplicationService  {
    /**
     * @param {import('@themost/express').ExpressDataApplication} app 
     */
    constructor(app) {
        super(app);
        if (app && app.serviceRouter) {
            // subscribe to serviceRouter
            app.serviceRouter.subscribe((serviceRouter) => {
                // try update scope access
                this.tryUpdateScopeAccess();
                // use user consent to service router
                const addRouter = Router();
                addRouter.use('/users/me/consents', userConsentServiceRouter(app));
                // insert router at the beginning of serviceRouter.stack
                serviceRouter.stack.unshift.apply(serviceRouter.stack, addRouter.stack);  
            });
        }
        // try update scope access when container is ready
        // this operation is very important when `UserConsentService` is being registered after the container is ready
        if (app && app.container) {
            app.container.subscribe((container) => {
                if (container) {
                    this.tryUpdateScopeAccess()
                }
            })
        }
    }

    tryUpdateScopeAccess() {
        const scopeAccess = this.application.getConfiguration().getStrategy(function ScopeAccessConfiguration() { });
        if (scopeAccess != null) {
            for (const element of LocalScopeAccessConfiguration) {
                if (scopeAccess.elements.findIndex((x) => x.resource === element.resource) === -1) {
                    scopeAccess.elements.push(element);
                }
            }
        }
    }

    /**
     * 
     * @param {import('@themost/data').DataContext} context 
     * @returns {import('./GenericConsentFieldGroup').GenericConsentFieldDocument} 
     */
    async getConsentsForCurrentUser(context) {
        return this.getConsentsForUser(context, context?.user?.name);
    }

    /**
     * 
     * @param {import('@themost/data').DataContext} context
     * @param {string} user
     * @returns {import('./GenericConsentFieldGroup').GenericConsentFieldDocument} 
     */
    async getConsentsForUser(context, user) {
        const [fieldGroups, values] = await Promise.sequence([
            () => context.model('ConsentFieldGroup').asQueryable().expand('fields').getAllItems(),
            () => context.model('UserConsent').where('user/name').equal(user).getAllItems()
        ]);
        return fieldGroups.reduce((acc, fieldGroup) => {
            const converted = new GenericConsentFieldGroupConverter(fieldGroup).convert(values);
            return Object.assign(acc, converted);
        }, {});
    }

    /**
     * Gets user consents for a specific group
     * @param {import('@themost/data').DataContext} context 
     * @param {string} user 
     * @param {string} group 
     * @returns {import('./GenericConsentFieldGroup').GenericConsentFieldDocument} 
     */
    async getConsentGroupForUser(context, user, group) {
        const [fieldGroup, values] = await Promise.sequence([
            () => context.model('ConsentFieldGroup').where('alternateName').equal(group).expand('fields').getItem(),
            () => context.model('UserConsent').where('user/name').equal(user).and('consent/group').equal(group).getAllItems()
        ]);
        if (fieldGroup == null) {
            throw new DataNotFoundError('The specified consent group cannot be found or is inaccessible', null, 'ConsentFieldGroup');
        } 
        return new GenericConsentFieldGroupConverter(fieldGroup).convert(values);
    }

    /**
     * Gets interactive user consents for a specific group
     * @param {import('@themost/data').DataContext} context 
     * @param {string} user 
     * @param {string} group 
     */
    async getConsentGroupForCurrentUser(context, group) {
        return this.getConsentGroupForUser(context, context?.user?.name, group);
    }

    /**
     * @param {import('@themost/data').DataContext} context
     * @param {{import('./GenericConsentFieldGroup').GenericConsentFieldDocument} consents 
     */
    async setConsentsForCurrentUser(context, consents) {
        const items = [];
        Object.keys(consents).filter((group) => {
            return Object.prototype.hasOwnProperty.call(consents, group);
        }).forEach((group) => {
            Object.keys(consents[group]).filter((field) => {
                return field !== 'metadata' && Object.prototype.hasOwnProperty.call(consents[group], field);
            }).forEach((consent) => {
                const {val: value} = consents[group][consent];
                const user = {
                    name: context.user.name,
                }
                items.push({
                    consent,
                    value,
                    user
                });
            });
        });
        await context.model('UserConsent').save(items);
        return items.length;
    }
}

export {
    UserConsentService
}